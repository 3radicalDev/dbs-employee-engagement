

# DBS Employee Engagement

** Clone this repo, and follow steps mentioned below to get started quickly.

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.


## To Use


```bash
# Clone this repository
git clone git@bitbucket.org:3radicalDev/dbs-employee-engagement.git
# Go into the repository
cd dbs-employee-engagement
# Install dependencies
npm install
# Run the app
npm start
```

## Link a website to display
Create symlink of webframework into the project root directory. Something like.
```
ln -s /Users/premkumar/Documents/vagrant/webframework .
```

## Changes in Webframework
The branch **electron-test** contains the changes but lets say you want to use NAB inside electron you will have to make changes in `override.js`, `index.html` and `debugindex.html`

### webframework/app/override.js
```
window.schemeId = 95
window.name = "NABDigifitChallenge"
window.skipDevicePrefix = true
window.apiAddress = "https://demo-nab2-app.vocohub.com"
```

### webframework/app/index.html & webframework/app/debugindex.html
Add **jquery** into head section of both html.
```
<script
  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>
```


### main.js
add link to ./webframework/app/debugindex.html